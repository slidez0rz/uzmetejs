"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pino_1 = __importDefault(require("pino"));
const transport = pino_1.default.transport({
    targets: [
        {
            target: 'pino/file',
            options: {
                destination: 'logs/error.log',
            },
            level: 'warn',
        },
        {
            target: 'pino-pretty',
            options: {
                destination: 1,
                translateTime: true,
                ignore: 'pid,hostname',
            },
            level: 'trace',
        },
    ],
});
const logger = (0, pino_1.default)(transport);
exports.default = logger;
