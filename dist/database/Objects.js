"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Connection_1 = __importDefault(require("./Connection"));
const User_1 = __importDefault(require("../models/User"));
const User = (0, User_1.default)(Connection_1.default);
exports.default = {
    User,
};
