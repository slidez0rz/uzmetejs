"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const Connection_1 = __importDefault(require("../database/Connection"));
class Config extends sequelize_1.Model {
}
Config.init({
    id: {
        type: sequelize_1.DataTypes.BIGINT.UNSIGNED,
        primaryKey: true,
    },
    guildId: {
        field: 'guild_id',
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    rankRoles: {
        field: 'rank_roles',
        type: sequelize_1.DataTypes.JSON,
        defaultValue: {},
    },
}, {
    sequelize: Connection_1.default,
    tableName: 'configs',
    indexes: [
        {
            fields: [
                'guild_id',
            ],
            unique: true,
        },
    ],
});
exports.default = Config;
