"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThrowLog = exports.Throw = exports.User = exports.Configuration = void 0;
const Configuration_1 = __importDefault(require("./Configuration"));
exports.Configuration = Configuration_1.default;
const Throw_1 = __importDefault(require("./Throw"));
exports.Throw = Throw_1.default;
const ThrowLog_1 = __importDefault(require("./ThrowLog"));
exports.ThrowLog = ThrowLog_1.default;
const User_1 = __importDefault(require("./User"));
exports.User = User_1.default;
User_1.default.hasMany(Throw_1.default, {
    sourceKey: 'id',
    foreignKey: 'user_id',
    as: 'throws',
});
User_1.default.hasMany(ThrowLog_1.default, {
    sourceKey: 'id',
    foreignKey: 'user_id',
    as: 'actions',
});
Throw_1.default.belongsTo(User_1.default, {
    foreignKey: 'user_id',
    as: 'user',
});
Throw_1.default.belongsTo(User_1.default, {
    foreignKey: 'author_id',
    as: 'author',
});
Throw_1.default.hasMany(ThrowLog_1.default, {
    sourceKey: 'id',
    foreignKey: 'throw_id',
    as: 'logs',
});
ThrowLog_1.default.belongsTo(Throw_1.default, {
    foreignKey: 'throw_id',
    as: 'throw',
});
ThrowLog_1.default.belongsTo(User_1.default, {
    foreignKey: 'author_id',
    as: 'author',
});
