"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const Connection_1 = __importDefault(require("../database/Connection"));
const Severity_1 = __importDefault(require("../enums/Severity"));
class Throw extends sequelize_1.Model {
}
Throw.init({
    id: {
        type: sequelize_1.DataTypes.BIGINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
    },
    guildId: {
        field: 'guild_id',
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    userId: {
        field: 'user_id',
        type: sequelize_1.DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
    },
    authorId: {
        field: 'author_id',
        type: sequelize_1.DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
    },
    reason: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    severity: {
        type: sequelize_1.DataTypes.ENUM,
        values: Object.values(Severity_1.default).map(String).slice(Object.values(Severity_1.default).length / 2),
        allowNull: false,
    },
    isDeleted: {
        field: 'is_deleted',
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: false,
    },
    createdAt: {
        field: 'created_at',
        type: sequelize_1.DataTypes.DATE,
    },
}, {
    sequelize: Connection_1.default,
    tableName: 'throws',
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    indexes: [
        {
            fields: [
                'guild_id',
                'created_at',
            ],
        },
        {
            fields: [
                'user_id',
                'created_at',
            ],
        },
        {
            fields: [
                'author_id',
                'created_at',
            ],
        },
    ],
});
exports.default = Throw;
