"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const Connection_1 = __importDefault(require("../database/Connection"));
class Configuration extends sequelize_1.Model {
}
Configuration.init({
    id: {
        type: sequelize_1.DataTypes.BIGINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
    },
    guildId: {
        field: 'guild_id',
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    guildName: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    isEnabled: {
        field: 'is_enabled',
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: false,
    },
    rankRoles: {
        field: 'rank_roles',
        type: sequelize_1.DataTypes.JSON,
        defaultValue: '[]',
        get: function () {
            // @ts-ignore
            return JSON.parse(this.getDataValue('rankRoles'));
        },
        set: function (value) {
            // @ts-ignore
            return this.setDataValue('rankRoles', JSON.stringify(value));
        },
    },
}, {
    sequelize: Connection_1.default,
    tableName: 'configurations',
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    indexes: [
        {
            fields: [
                'guild_id',
            ],
            unique: true,
        },
    ],
});
exports.default = Configuration;
