"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const Connection_1 = __importDefault(require("../database/Connection"));
const ThrowAction_1 = __importDefault(require("../enums/ThrowAction"));
class ThrowLog extends sequelize_1.Model {
}
ThrowLog.init({
    id: {
        type: sequelize_1.DataTypes.BIGINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
    },
    guildId: {
        field: 'guild_id',
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    throwId: {
        field: 'throw_id',
        type: sequelize_1.DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
    },
    authorId: {
        field: 'author_id',
        type: sequelize_1.DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
    },
    action: {
        type: sequelize_1.DataTypes.ENUM,
        values: Object.values(ThrowAction_1.default).map(String).slice(Object.values(ThrowAction_1.default).length / 2),
        allowNull: false,
    },
}, {
    sequelize: Connection_1.default,
    tableName: 'throw_logs',
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    indexes: [
        {
            fields: [
                'guild_id',
                'created_at',
            ],
        },
        {
            fields: [
                'author_id',
                'created_at',
            ],
        },
    ],
});
exports.default = ThrowLog;
