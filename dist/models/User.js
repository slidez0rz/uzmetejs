"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const Connection_1 = __importDefault(require("../database/Connection"));
class User extends sequelize_1.Model {
}
User.init({
    id: {
        type: sequelize_1.DataTypes.BIGINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
    },
    guildId: {
        field: 'guild_id',
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    guildMemberId: {
        field: 'guild_member_id',
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    guildMemberName: {
        field: 'guild_member_name',
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    points: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        defaultValue: 0,
    },
}, {
    sequelize: Connection_1.default,
    tableName: 'users',
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    indexes: [
        {
            fields: [
                'guild_id',
                'points',
            ],
        },
        {
            fields: [
                'guild_member_id',
            ],
            unique: true,
        },
    ],
});
exports.default = User;
