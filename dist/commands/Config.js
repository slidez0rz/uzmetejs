"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Base_1 = __importDefault(require("./Base"));
const discord_js_1 = require("discord.js");
const builders_1 = require("@discordjs/builders");
const Logger_1 = __importDefault(require("../utils/Logger"));
const GuildManager_1 = __importDefault(require("../managers/GuildManager"));
const ThrowManager_1 = __importDefault(require("../managers/ThrowManager"));
class Config extends Base_1.default {
    constructor() {
        super(...arguments);
        this.name = 'uzmetējs';
    }
    getCommand() {
        const builder = new builders_1.SlashCommandBuilder();
        builder.setName(this.name);
        builder.setDescription('Konfigurēt uzmetēju');
        builder.addSubcommand((subCommand) => {
            subCommand.setName('ieslēgts');
            subCommand.setDescription('Ieslēgt uzmetēju');
            return subCommand;
        });
        builder.addSubcommand((subCommand) => {
            subCommand.setName('izslēgts');
            subCommand.setDescription('Izslēgt uzmetēju');
            return subCommand;
        });
        builder.addSubcommand((subCommand) => {
            subCommand.setName('vietas');
            subCommand.setDescription('Norādīt kādu role saņem uzmetējs');
            subCommand.addIntegerOption((option) => {
                option.setName('ranks');
                option.setDescription('Uzmetēja vieta topā');
                option.setRequired(true);
                return option;
            });
            subCommand.addRoleOption((option) => {
                option.setName('role');
                option.setDescription('Role, ko saņems uzmetējs');
                return option;
            });
            return subCommand;
        });
        builder.addSubcommand((subCommand) => {
            subCommand.setName('calculate');
            subCommand.setDescription('Pārdala roles');
            return subCommand;
        });
        return builder;
    }
    execute(interaction) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            if (!((_a = interaction.memberPermissions) === null || _a === void 0 ? void 0 : _a.has(discord_js_1.Permissions.FLAGS.ADMINISTRATOR)) || !interaction.guild) {
                yield interaction.reply({
                    ephemeral: true,
                    content: 'Kur līdīsi?',
                });
                return;
            }
            switch (interaction.options.getSubcommand()) {
                case 'ieslēgts':
                    if (yield GuildManager_1.default.enable(interaction.guild)) {
                        yield interaction.reply({
                            ephemeral: true,
                            content: 'Uzmetējs aktivizēts!',
                        });
                        return;
                    }
                    yield interaction.reply({
                        ephemeral: true,
                        content: 'Kaut kas nestrādā!',
                    });
                    return;
                case 'izslēgts':
                    if (yield GuildManager_1.default.disable(interaction.guild)) {
                        yield interaction.reply({
                            ephemeral: true,
                            content: 'Uzmetējs izlēgts!',
                        });
                        return;
                    }
                    yield interaction.reply({
                        ephemeral: true,
                        content: 'Kaut kas nestrādā!',
                    });
                    return;
                case 'vietas':
                    if (yield GuildManager_1.default.updateRank(interaction.guild, interaction.options.getInteger('ranks', true), interaction.options.getRole('role'))) {
                        yield interaction.reply({
                            ephemeral: true,
                            content: `Atjaunots role ${interaction.options.getInteger('ranks')}. vietai!`,
                        });
                        return;
                    }
                    yield interaction.reply({
                        ephemeral: true,
                        content: 'Kaut kas nestrādā!',
                    });
                    break;
                case 'calculate':
                    yield ThrowManager_1.default.calculate(interaction.guild);
                    yield interaction.reply({
                        ephemeral: true,
                        content: 'Done!',
                    });
                    return;
                default:
                    Logger_1.default.error(`Invalid subcommand "${interaction.options.getSubcommand()}" for ${this.name}`);
                    yield interaction.reply({
                        ephemeral: true,
                        content: 'Nav īstā komanda!',
                    });
            }
        });
    }
}
exports.default = Config;
