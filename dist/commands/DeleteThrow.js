"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Base_1 = __importDefault(require("./Base"));
const builders_1 = require("@discordjs/builders");
const discord_js_1 = require("discord.js");
const models_1 = require("../models");
const ThrowManager_1 = __importDefault(require("../managers/ThrowManager"));
class DeleteThrow extends Base_1.default {
    constructor() {
        super(...arguments);
        this.name = 'neuzmeta';
    }
    getCommand() {
        const builder = new builders_1.SlashCommandBuilder();
        builder.setName(this.name);
        builder.setDescription('Nodzēš uzmetienu');
        builder.addIntegerOption((option) => {
            option.setName('uzmetiens');
            option.setDescription('Uzmetiena ID');
            option.setRequired(true);
            return option;
        });
        return builder;
    }
    execute(interaction) {
        var _a, _b, _c;
        return __awaiter(this, void 0, void 0, function* () {
            const throwId = interaction.options.getInteger('uzmetiens', true);
            const throwModel = yield models_1.Throw.findOne({
                where: {
                    id: throwId,
                },
                include: [
                    {
                        model: models_1.User,
                        as: 'user',
                    },
                    {
                        model: models_1.User,
                        as: 'author',
                    },
                ],
            });
            if (!throwModel) {
                yield interaction.reply({
                    ephemeral: true,
                    content: 'Uzmetiens nav atrasts!',
                });
                return;
            }
            if (throwModel.isDeleted) {
                yield interaction.reply({
                    ephemeral: true,
                    content: 'Uzmetiens jau ir izdzēsts!',
                });
                return;
            }
            const guildMember = interaction.member;
            if (((_a = throwModel.author) === null || _a === void 0 ? void 0 : _a.guildMemberId) !== guildMember.id && !((_b = interaction.memberPermissions) === null || _b === void 0 ? void 0 : _b.has(discord_js_1.Permissions.FLAGS.ADMINISTRATOR))) {
                if (((_c = throwModel.user) === null || _c === void 0 ? void 0 : _c.guildMemberId) === guildMember.id) {
                    yield interaction.reply({
                        content: `${guildMember} cenšas appist sistēmu un noņemt sev uzmetienu!`,
                    });
                }
                else {
                    yield interaction.reply({
                        ephemeral: true,
                        content: 'Nevari noņemt cita veidotu uzmetienu!',
                    });
                }
                return;
            }
            yield ThrowManager_1.default.removeThrow(throwModel);
            yield interaction.reply({
                content: `${interaction.member} nodzēsa uzmetienu #${throwId}`,
            });
        });
    }
}
exports.default = DeleteThrow;
