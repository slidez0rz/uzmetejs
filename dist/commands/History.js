"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Base_1 = __importDefault(require("./Base"));
const builders_1 = require("@discordjs/builders");
const discord_js_1 = require("discord.js");
const models_1 = require("../models");
class History extends Base_1.default {
    constructor() {
        super(...arguments);
        this.name = 'uzmetieni';
    }
    getCommand() {
        const builder = new builders_1.SlashCommandBuilder();
        builder.setName(this.name);
        builder.setDescription('Parāda pēdējos uzmetienus');
        builder.addUserOption(option => option.setName('uzmetējs')
            .setDescription('Uzmetējs'));
        builder.addIntegerOption(option => option.setName('lapa')
            .setDescription('Lapa')
            .setMinValue(1));
        builder.addIntegerOption(option => option.setName('uzmetiens')
            .setDescription('Uzmetiena ID')
            .setMinValue(1));
        return builder;
    }
    execute(interaction) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            if (!interaction.guildId) {
                yield interaction.reply({
                    ephemeral: true,
                    content: 'WTF?',
                });
                return;
            }
            const severityTexts = [
                '',
                'nedaudz',
                'vidēji',
                'smagi',
            ];
            if (interaction.options.getInteger('uzmetiens')) {
                const throwModel = yield models_1.Throw.findOne({
                    where: {
                        id: interaction.options.getInteger('uzmetiens'),
                    },
                    include: [
                        {
                            association: 'user',
                        },
                        {
                            association: 'author',
                        },
                    ],
                });
                if (!throwModel) {
                    yield interaction.reply({
                        ephemeral: true,
                        content: 'Uzmetiens nav atrasts',
                    });
                    return;
                }
                yield interaction.reply({
                    ephemeral: true,
                    embeds: [
                        new discord_js_1.MessageEmbed()
                            .setColor('RANDOM')
                            .setTitle(`Uzmetiens #${throwModel.id}`)
                            .setDescription(`Izskatās, ka ${(_a = throwModel.user) === null || _a === void 0 ? void 0 : _a.guildMemberName} ir visus ${severityTexts[throwModel.severity]} uzmetis!`)
                            .addField('Iemesls', throwModel.reason)
                            .setTimestamp(throwModel.createdAt.getTime())
                            .setFooter(`Uzmetienu reģistrēja ${(_b = throwModel.author) === null || _b === void 0 ? void 0 : _b.guildMemberName}`),
                    ],
                });
                return;
            }
            const guildMember = interaction.options.getMember('uzmetējs');
            const page = interaction.options.getInteger('lapa') || 1;
            let where = {};
            if (guildMember && guildMember instanceof discord_js_1.GuildMember) {
                const user = yield models_1.User.findOne({
                    where: {
                        guildMemberId: guildMember.id,
                    },
                });
                if (!user) {
                    yield interaction.reply({
                        ephemeral: true,
                        content: 'Uzmetējs nav atrasts',
                    });
                    return;
                }
                where = {
                    userId: user.id,
                };
            }
            const throws = yield models_1.Throw.findAndCountAll({
                where: where,
                order: [
                    ['created_at', 'DESC'],
                ],
                limit: 5,
                offset: (page - 1) * 5,
                include: [
                    {
                        association: 'user',
                    },
                    {
                        association: 'author',
                    },
                ],
            });
            if (!throws.rows.length || !throws.count) {
                yield interaction.reply({
                    ephemeral: true,
                    content: 'Netika atrasts neviens uzmetiens',
                });
                return;
            }
            const embeds = [];
            throws.rows.forEach((throwModel) => {
                var _a;
                const embed = new discord_js_1.MessageEmbed()
                    .setColor('RANDOM')
                    .setTitle(`Uzmetiens #${throwModel.id}`)
                    .setDescription(`Izskatās, ka ${(_a = throwModel.user) === null || _a === void 0 ? void 0 : _a.guildMemberName} ir visus ${severityTexts[throwModel.severity]} uzmetis!`)
                    .addField('Iemesls', throwModel.reason);
                if (throwModel.isDeleted) {
                    embed.addField('Uzmetiena statuss', 'Dzēsts');
                }
                embeds.push(embed);
            });
            embeds.push(new discord_js_1.MessageEmbed()
                .setColor('RANDOM')
                .setDescription(`${page}. lapa no ${Math.ceil(throws.count / 5)}`));
            yield interaction.reply({
                ephemeral: true,
                embeds: embeds,
            });
        });
    }
}
exports.default = History;
