"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Base_1 = __importDefault(require("./Base"));
const builders_1 = require("@discordjs/builders");
const discord_js_1 = require("discord.js");
const models_1 = require("../models");
const os_1 = require("os");
const sequelize_1 = require("sequelize");
class Top extends Base_1.default {
    constructor() {
        super(...arguments);
        this.name = 'uzmetēji';
    }
    getCommand() {
        const builder = new builders_1.SlashCommandBuilder();
        builder.setName(this.name);
        builder.setDescription('Parāda uzmetēju topu');
        return builder;
    }
    execute(interaction) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!interaction.guildId || !interaction.guild) {
                yield interaction.reply({
                    ephemeral: true,
                    content: 'WTF?',
                });
                return;
            }
            const users = yield models_1.User.findAll({
                where: {
                    guildId: interaction.guild.id,
                    points: {
                        [sequelize_1.Op.gt]: 0,
                    },
                },
                order: [
                    ['points', 'DESC'],
                    ['updated_at', 'DESC'],
                ],
            });
            if (!users.length) {
                yield interaction.reply({
                    ephemeral: true,
                    content: 'Neviens nav uzmetis!',
                });
                return;
            }
            const ranks = [];
            const names = [];
            const points = [];
            users.forEach((user, index) => {
                ranks.push(index + 1);
                names.push(user.guildMemberName);
                points.push(user.points);
            });
            yield interaction.reply({
                embeds: [
                    new discord_js_1.MessageEmbed()
                        .setColor('RANDOM')
                        .setTitle('Lielākie uzmetēji')
                        .addFields([
                        {
                            name: '#',
                            value: ranks.join(os_1.EOL),
                            inline: true,
                        },
                        {
                            name: 'Uzmetējs',
                            value: names.join(os_1.EOL),
                            inline: true,
                        },
                        {
                            name: 'Punkti',
                            value: points.join(os_1.EOL),
                            inline: true,
                        },
                    ])
                        .setTimestamp(),
                ],
            });
        });
    }
}
exports.default = Top;
