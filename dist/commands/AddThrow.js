"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Base_1 = __importDefault(require("./Base"));
const builders_1 = require("@discordjs/builders");
const Severity_1 = __importDefault(require("../enums/Severity"));
const discord_js_1 = require("discord.js");
const models_1 = require("../models");
const ThrowAction_1 = __importDefault(require("../enums/ThrowAction"));
const ThrowManager_1 = __importDefault(require("../managers/ThrowManager"));
class AddThrow extends Base_1.default {
    constructor() {
        super(...arguments);
        this.name = 'uzmetiens';
    }
    getCommand() {
        const builder = new builders_1.SlashCommandBuilder();
        builder.setName(this.name);
        builder.setDescription('Izveido jaunu uzmetienu izvēlētajam lietotājam');
        builder.addUserOption(option => option.setName('uzmetējs')
            .setDescription('Kurš uzmeta?')
            .setRequired(true));
        builder.addStringOption(option => option.setName('iemesls')
            .setDescription('Kāpēc uzmeta?')
            .setRequired(true));
        builder.addIntegerOption(option => option.setName('smagums')
            .setDescription('Cik smags uzmetiens?')
            .addChoice('Mazs', Severity_1.default.Low)
            .addChoice('Vidējs', Severity_1.default.Medium)
            .addChoice('Liels', Severity_1.default.High));
        return builder;
    }
    execute(interaction) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            if (!interaction.guildId || !interaction.guild) {
                yield interaction.reply({
                    ephemeral: true,
                    content: 'WTF?',
                });
                return;
            }
            const member = interaction.options.getMember('uzmetējs', true);
            if (member.user.bot) {
                yield interaction.reply({
                    ephemeral: true,
                    content: 'Nevari uzmest botu!',
                });
                return;
            }
            yield interaction.deferReply();
            const lastThrow = yield models_1.Throw.findOne({
                order: [
                    ['id', 'DESC'],
                ],
                limit: 1,
                include: [
                    {
                        association: 'author',
                    },
                ],
            });
            if (lastThrow && ((_a = lastThrow.author) === null || _a === void 0 ? void 0 : _a.guildMemberId) === member.id && !((_b = interaction.memberPermissions) === null || _b === void 0 ? void 0 : _b.has(discord_js_1.Permissions.FLAGS.ADMINISTRATOR))) {
                yield interaction.reply({
                    content: 'Why you bully me!?',
                });
                return;
            }
            const reason = interaction.options.getString('iemesls', true);
            const severity = interaction.options.getInteger('smagums') || Severity_1.default.Low;
            const [userModel] = yield models_1.User.findCreateFind({
                where: {
                    guildMemberId: member.id,
                },
                defaults: {
                    guildId: interaction.guildId,
                    guildMemberId: member.id,
                    guildMemberName: member.displayName,
                },
            });
            const authorMember = interaction.member;
            const [authorModel] = yield models_1.User.findCreateFind({
                where: {
                    guildMemberId: authorMember.id,
                },
                defaults: {
                    guildId: interaction.guildId,
                    guildMemberId: authorMember.id,
                    guildMemberName: authorMember.displayName,
                },
            });
            const throwModel = yield userModel.createThrow({
                guildId: interaction.guildId,
                userId: userModel.id,
                authorId: authorModel.id,
                severity: severity,
                reason: reason,
            });
            yield throwModel.createLog({
                throwId: throwModel.id,
                guildId: interaction.guildId,
                authorId: authorModel.id,
                action: ThrowAction_1.default.Create,
            });
            yield userModel.increment('points', {
                by: severity,
            });
            const severityTexts = [
                '',
                'nedaudz',
                'vidēji',
                'smagi',
            ];
            yield interaction.editReply({
                embeds: [
                    new discord_js_1.MessageEmbed()
                        .setColor('RANDOM')
                        .setTitle('Jauns uzmetiens!')
                        .setDescription(`Izskatās, ka ${member} ir visus ${severityTexts[severity]} uzmetis!`)
                        .addField('Iemesls', reason)
                        .setTimestamp()
                        .setFooter(`Uzmetiena ID #${throwModel.id}`),
                ],
            });
            yield ThrowManager_1.default.calculate(interaction.guild);
        });
    }
}
exports.default = AddThrow;
