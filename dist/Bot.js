"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_1 = require("discord.js");
const Logger_1 = __importDefault(require("./utils/Logger"));
const Helpers_1 = require("./utils/Helpers");
const GuildManager_1 = __importDefault(require("./managers/GuildManager"));
const ThrowManager_1 = __importDefault(require("./managers/ThrowManager"));
class Bot extends discord_js_1.Client {
    constructor() {
        super({
            intents: [
                discord_js_1.Intents.FLAGS.GUILDS,
            ],
        });
        this.loginTries = 0;
        this.commands = new discord_js_1.Collection();
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            GuildManager_1.default.setClient(this);
            ThrowManager_1.default.setClient(this);
            this.commands = yield (0, Helpers_1.loadCommands)(this);
            yield (0, Helpers_1.registerEvents)(this);
            this.once('ready', this.onReady);
            this.on('interactionCreate', (interaction) => __awaiter(this, void 0, void 0, function* () {
                if (!interaction.isCommand()) {
                    return;
                }
                const command = this.commands.get(interaction.commandName);
                if (!command) {
                    Logger_1.default.warn(`Invalid command "${interaction.commandName}" specified`);
                    return;
                }
                try {
                    yield command.execute(interaction);
                }
                catch (error) {
                    Logger_1.default.error(error);
                    yield interaction.reply({
                        content: 'There was an error while executing this command!',
                        ephemeral: true,
                    });
                }
            }));
            this.tryLogin();
        });
    }
    tryLogin() {
        this.loginTries++;
        super.login(process.env.BOT_TOKEN).catch(e => {
            if (this.loginTries < 3) {
                this.tryLogin();
            }
            else {
                Logger_1.default.error(e.message);
                Logger_1.default.info('Failed to authorize bot. Retrying in 60 seconds');
                this.loginTries = 0;
                setTimeout(this.tryLogin, 60000);
            }
        });
    }
    onReady() {
        return __awaiter(this, void 0, void 0, function* () {
            this.guilds.cache.each((guild) => {
                GuildManager_1.default.init(guild);
            });
            Logger_1.default.info('Ready!');
        });
    }
}
exports.default = Bot;
