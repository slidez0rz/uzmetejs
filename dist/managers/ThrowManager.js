"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../models");
const Logger_1 = __importDefault(require("../utils/Logger"));
const sequelize_1 = require("sequelize");
class ThrowManager {
    setClient(client) {
        this.client = client;
    }
    calculate(guild) {
        return __awaiter(this, void 0, void 0, function* () {
            const config = yield models_1.Configuration.findOne({
                where: {
                    guildId: guild.id,
                },
            });
            if (!config) {
                return;
            }
            const activeUsers = yield models_1.User.findAll({
                where: {
                    guildId: guild.id,
                },
                order: [
                    ['points', 'DESC'],
                    ['updated_at', 'DESC'],
                ],
            });
            for (const user of activeUsers) {
                let member;
                try {
                    member = yield guild.members.fetch(user.guildMemberId);
                    if (!member) {
                        continue;
                    }
                }
                catch (e) {
                    Logger_1.default.error(e);
                    continue;
                }
                const rank = activeUsers.findIndex(activeUser => activeUser.id === user.id) + 1;
                yield this.setMemberRankRoles(config, member, user, rank);
            }
        });
    }
    setRole(member) {
        return __awaiter(this, void 0, void 0, function* () {
            const config = yield models_1.Configuration.findOne({
                where: {
                    guildId: member.guild.id,
                },
            });
            if (!config) {
                return;
            }
            const user = yield models_1.User.findOne({
                where: {
                    guildMemberId: member.id,
                },
            });
            if (!user) {
                return;
            }
            if (!user.points) {
                return;
            }
            const rank = (yield models_1.User.count({
                where: {
                    guildId: member.guild.id,
                    [sequelize_1.Op.or]: [
                        {
                            points: {
                                [sequelize_1.Op.gt]: user.points,
                            },
                        },
                        {
                            points: user.points,
                            updated_at: {
                                [sequelize_1.Op.gt]: user.updatedAt,
                            },
                        },
                    ],
                },
            })) + 1;
            yield this.setMemberRankRoles(config, member, user, rank);
        });
    }
    setMemberRankRoles(config, member, user, rank) {
        return __awaiter(this, void 0, void 0, function* () {
            const roleIds = config.rankRoles.map(rankRole => rankRole.roleId);
            const activeRankRole = config.rankRoles.find(rankRole => rankRole.rank === rank);
            let rolesToRemove = roleIds;
            if (activeRankRole && user.points) {
                rolesToRemove = rolesToRemove.filter(roleId => roleId !== activeRankRole.roleId);
            }
            try {
                yield member.roles.remove(rolesToRemove);
                if (activeRankRole && user.points) {
                    yield member.roles.add(activeRankRole.roleId);
                }
            }
            catch (e) {
                Logger_1.default.error(e);
            }
        });
    }
    removeThrow(throwModel) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const points = throwModel.severity;
            yield throwModel.update({
                isDeleted: true,
            });
            yield ((_a = throwModel.user) === null || _a === void 0 ? void 0 : _a.decrement('points', {
                by: points,
            }));
            const guild = yield this.client.guilds.fetch(throwModel.guildId);
            if (!guild) {
                return;
            }
            yield this.calculate(guild);
        });
    }
}
const throwManager = new ThrowManager();
exports.default = throwManager;
