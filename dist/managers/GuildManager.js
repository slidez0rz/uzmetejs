"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../models");
const v9_1 = require("discord-api-types/v9");
const Helpers_1 = require("../utils/Helpers");
const Values_1 = require("../utils/Values");
const rest_1 = require("@discordjs/rest");
const Logger_1 = __importDefault(require("../utils/Logger"));
const token = String(process.env.BOT_TOKEN);
const clientId = String(process.env.CLIENT_ID);
class GuildManager {
    setClient(client) {
        this.client = client;
    }
    init(guild) {
        return __awaiter(this, void 0, void 0, function* () {
            let config = yield models_1.Configuration.findOne({
                where: {
                    guildId: guild.id,
                },
            });
            if (!config) {
                config = yield models_1.Configuration.create({
                    guildId: guild.id,
                    guildName: guild.name,
                });
            }
            if (!config.isEnabled) {
                yield this.unregisterGuildCommands(guild);
                return;
            }
            yield this.registerGuildCommands(guild);
        });
    }
    enable(guild) {
        return __awaiter(this, void 0, void 0, function* () {
            const config = yield models_1.Configuration.findOne({
                where: {
                    guildId: guild.id,
                },
            });
            if (!config) {
                return false;
            }
            yield config.update({
                isEnabled: true,
            });
            yield this.registerGuildCommands(guild);
            return true;
        });
    }
    disable(guild) {
        return __awaiter(this, void 0, void 0, function* () {
            const config = yield models_1.Configuration.findOne({
                where: {
                    guildId: guild.id,
                },
            });
            if (!config) {
                return false;
            }
            yield config.update({
                isEnabled: false,
            });
            yield this.unregisterGuildCommands(guild);
            return true;
        });
    }
    updateRank(guild, rank, role) {
        return __awaiter(this, void 0, void 0, function* () {
            const config = yield models_1.Configuration.findOne({
                where: {
                    guildId: guild.id,
                },
            });
            if (!config) {
                return false;
            }
            const rankRoles = config.rankRoles;
            const currentIndex = config.rankRoles.findIndex(rankRole => rankRole.rank === rank);
            if (role === null) {
                if (currentIndex) {
                    return true;
                }
                rankRoles.splice(currentIndex, 1);
                yield config.update({
                    rankRoles: rankRoles,
                });
                return true;
            }
            const rankRole = {
                rank: rank,
                roleId: role.id,
            };
            if (currentIndex === -1) {
                rankRoles.push(rankRole);
            }
            else {
                rankRoles[currentIndex] = rankRole;
            }
            rankRoles.sort((a, b) => {
                return a.rank - b.rank;
            });
            yield config.update({
                rankRoles: rankRoles,
            });
            return true;
        });
    }
    registerGuildCommands(guild) {
        return __awaiter(this, void 0, void 0, function* () {
            const rest = new rest_1.REST({
                version: '9',
            }).setToken(token);
            const commands = (yield (0, Helpers_1.loadCommands)())
                .filter((command) => !Values_1.globalCommands.includes(command.name))
                .map((command) => command.getCommand().toJSON());
            rest.put(v9_1.Routes.applicationGuildCommands(clientId, guild.id), {
                body: commands,
            }).then(() => {
                Logger_1.default.info(`Guild commands registered for guild ${guild.name}`);
            }).catch(e => {
                Logger_1.default.info(e.message);
            });
        });
    }
    unregisterGuildCommands(guild) {
        const rest = new rest_1.REST({
            version: '9',
        }).setToken(token);
        rest.put(v9_1.Routes.applicationGuildCommands(clientId, guild.id), {
            body: [],
        }).then(() => {
            Logger_1.default.info(`Guild commands unregistered for guild ${guild.name}`);
        }).catch(e => {
            Logger_1.default.info(e.message);
        });
    }
}
const guildManager = new GuildManager();
exports.default = guildManager;
