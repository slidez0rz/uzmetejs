"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Severity;
(function (Severity) {
    Severity[Severity["Low"] = 1] = "Low";
    Severity[Severity["Medium"] = 2] = "Medium";
    Severity[Severity["High"] = 3] = "High";
})(Severity || (Severity = {}));
exports.default = Severity;
