"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ThrowAction;
(function (ThrowAction) {
    ThrowAction["Create"] = "create";
    ThrowAction["Delete"] = "delete";
})(ThrowAction || (ThrowAction = {}));
exports.default = ThrowAction;
