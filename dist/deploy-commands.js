"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const rest_1 = require("@discordjs/rest");
const Logger_1 = __importDefault(require("./utils/Logger"));
const Helpers_1 = require("./utils/Helpers");
const v9_1 = require("discord-api-types/v9");
const Values_1 = require("./utils/Values");
if (!process.env.BOT_TOKEN) {
    Logger_1.default.debug('No BOT_TOKEN set!');
    process.exit(1);
}
if (!process.env.CLIENT_ID) {
    Logger_1.default.debug('No BOT_TOKEN set!');
    process.exit(1);
}
const token = String(process.env.BOT_TOKEN);
const clientId = String(process.env.CLIENT_ID);
const rest = new rest_1.REST({
    version: '9',
}).setToken(token);
(function () {
    return __awaiter(this, void 0, void 0, function* () {
        const commands = yield (0, Helpers_1.loadCommands)();
        rest.put(v9_1.Routes.applicationCommands(clientId), {
            body: commands
                .filter((command) => Values_1.globalCommands.includes(command.name))
                .map((command) => command.getCommand().toJSON()),
        }).then(() => {
            Logger_1.default.info('Global commands registered');
        }).catch(e => {
            Logger_1.default.info(e.message);
        });
    });
})();
