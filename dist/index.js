"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const Bot_1 = __importDefault(require("./Bot"));
const Logger_1 = __importDefault(require("./utils/Logger"));
new Bot_1.default().init().catch(e => Logger_1.default.error(e.message));
