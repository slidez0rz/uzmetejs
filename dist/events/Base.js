"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Base {
    constructor(client) {
        if (client) {
            this.client = client;
        }
    }
}
exports.default = Base;
