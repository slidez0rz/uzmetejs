import Base from './Base';
import { SlashCommandBuilder } from '@discordjs/builders';
import { CommandInteraction } from 'discord.js';

class Paliidz extends Base {
	name = 'paliidz';

	getCommand(): SlashCommandBuilder {
		const builder = new SlashCommandBuilder();
		builder.setName(this.name);
		builder.setDescription('Palīīīdz');
		return builder;
	}

	async execute(interaction: CommandInteraction) {
		await interaction.reply({
			content: 'https://www.youtube.com/watch?v=5CFv2tlUmPg',
		});
	}
}

export default Paliidz;