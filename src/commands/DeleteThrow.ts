import Base from './Base';
import { SlashCommandBuilder, SlashCommandIntegerOption } from '@discordjs/builders';
import { CommandInteraction, GuildMember, Permissions } from 'discord.js';
import { Throw, User } from '../models';
import ThrowManager from '../managers/ThrowManager';

class DeleteThrow extends Base {
	name = 'neuzmeta';

	getCommand(): SlashCommandBuilder {
		const builder = new SlashCommandBuilder();
		builder.setName(this.name);
		builder.setDescription('Nodzēš uzmetienu');
		builder.addIntegerOption((option: SlashCommandIntegerOption) => {
			option.setName('uzmetiens');
			option.setDescription('Uzmetiena ID');
			option.setRequired(true);
			return option;
		});
		return builder;
	}

	async execute(interaction: CommandInteraction) {
		const throwId = interaction.options.getInteger('uzmetiens', true);
		const throwModel = await Throw.findOne({
			where: {
				id: throwId,
			},
			include: [
				{
					model: User,
					as: 'user',
				},
				{
					model: User,
					as: 'author',
				},
			],
		});
		if (!throwModel) {
			await interaction.reply({
				ephemeral: true,
				content: 'Uzmetiens nav atrasts!',
			});
			return;
		}
		if (throwModel.isDeleted) {
			await interaction.reply({
				ephemeral: true,
				content: 'Uzmetiens jau ir izdzēsts!',
			});
			return;
		}
		const guildMember = interaction.member as GuildMember;
		if (throwModel.author?.guildMemberId !== guildMember.id && !interaction.memberPermissions?.has(Permissions.FLAGS.ADMINISTRATOR)) {
			if (throwModel.user?.guildMemberId === guildMember.id) {
				await interaction.reply({
					content: `${guildMember} cenšas appist sistēmu un noņemt sev uzmetienu!`,
				});
			} else {
				await interaction.reply({
					ephemeral: true,
					content: 'Nevari noņemt cita veidotu uzmetienu!',
				});
			}
			return;
		}
		await ThrowManager.removeThrow(throwModel);
		await interaction.reply({
			content: `${interaction.member} nodzēsa uzmetienu #${throwId}`,
		});
	}
}

export default DeleteThrow;