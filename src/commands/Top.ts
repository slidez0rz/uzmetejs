import Base from './Base';
import { SlashCommandBuilder } from '@discordjs/builders';
import { CommandInteraction, MessageEmbed } from 'discord.js';
import { User } from '../models';
import { EOL } from 'os';
import { Op } from 'sequelize';

class Top extends Base {
	name = 'uzmetēji';

	getCommand(): SlashCommandBuilder {
		const builder = new SlashCommandBuilder();
		builder.setName(this.name);
		builder.setDescription('Parāda uzmetēju topu');
		return builder;
	}

	async execute(interaction: CommandInteraction) {
		if (!interaction.guildId || !interaction.guild) {
			await interaction.reply({
				ephemeral: true,
				content: 'WTF?',
			});
			return;
		}

		const users = await User.findAll({
			where: {
				guildId: interaction.guild.id,
				points: {
					[Op.gt]: 0,
				},
			},
			order: [
				['points', 'DESC'],
				['updated_at', 'DESC'],
			],
		});

		if (!users.length) {
			await interaction.reply({
				ephemeral: true,
				content: 'Neviens nav uzmetis!',
			});
			return;
		}

		const ranks: Array<number> = [];
		const names: Array<string> = [];
		const points: Array<number> = [];

		users.forEach((user: User, index: number) => {
			ranks.push(index + 1);
			names.push(user.guildMemberName);
			points.push(user.points);
		});

		await interaction.reply({
			embeds: [
				new MessageEmbed()
					.setColor('RANDOM')
					.setTitle('Lielākie uzmetēji')
					.addFields([
						{
							name: '#',
							value: ranks.join(EOL),
							inline: true,
						},
						{
							name: 'Uzmetējs',
							value: names.join(EOL),
							inline: true,
						},
						{
							name: 'Punkti',
							value: points.join(EOL),
							inline: true,
						},
					])
					.setTimestamp(),
			],
		});
	}
}

export default Top;