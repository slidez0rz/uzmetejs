import Base from './Base';
import { CommandInteraction, Permissions } from 'discord.js';
import {
	SlashCommandBuilder,
	SlashCommandIntegerOption,
	SlashCommandRoleOption,
	SlashCommandSubcommandBuilder,
} from '@discordjs/builders';
import logger from '../utils/Logger';
import GuildManager from '../managers/GuildManager';
import throwManager from '../managers/ThrowManager';

class Config extends Base {
	public name = 'uzmetējs';

	public getCommand(): SlashCommandBuilder {
		const builder = new SlashCommandBuilder();
		builder.setName(this.name);
		builder.setDescription('Konfigurēt uzmetēju');
		builder.addSubcommand((subCommand: SlashCommandSubcommandBuilder) => {
			subCommand.setName('ieslēgts');
			subCommand.setDescription('Ieslēgt uzmetēju');
			return subCommand;
		});
		builder.addSubcommand((subCommand: SlashCommandSubcommandBuilder) => {
			subCommand.setName('izslēgts');
			subCommand.setDescription('Izslēgt uzmetēju');
			return subCommand;
		});
		builder.addSubcommand((subCommand: SlashCommandSubcommandBuilder) => {
			subCommand.setName('vietas');
			subCommand.setDescription('Norādīt kādu role saņem uzmetējs');
			subCommand.addIntegerOption((option: SlashCommandIntegerOption) => {
				option.setName('ranks');
				option.setDescription('Uzmetēja vieta topā');
				option.setRequired(true);
				return option;
			});
			subCommand.addRoleOption((option: SlashCommandRoleOption) => {
				option.setName('role');
				option.setDescription('Role, ko saņems uzmetējs');
				return option;
			});
			return subCommand;
		});
		builder.addSubcommand((subCommand: SlashCommandSubcommandBuilder) => {
			subCommand.setName('calculate');
			subCommand.setDescription('Pārdala roles');
			return subCommand;
		});
		return builder;
	}

	public async execute(interaction: CommandInteraction) {
		if (!interaction.memberPermissions?.has(Permissions.FLAGS.ADMINISTRATOR) || !interaction.guild) {
			await interaction.reply({
				ephemeral: true,
				content: 'Kur līdīsi?',
			});
			return;
		}

		switch (interaction.options.getSubcommand()) {
			case 'ieslēgts':
				if (await GuildManager.enable(interaction.guild)) {
					await interaction.reply({
						ephemeral: true,
						content: 'Uzmetējs aktivizēts!',
					});
					return;
				}
				await interaction.reply({
					ephemeral: true,
					content: 'Kaut kas nestrādā!',
				});
				return;
			case 'izslēgts':
				if (await GuildManager.disable(interaction.guild)) {
					await interaction.reply({
						ephemeral: true,
						content: 'Uzmetējs izlēgts!',
					});
					return;
				}
				await interaction.reply({
					ephemeral: true,
					content: 'Kaut kas nestrādā!',
				});
				return;
			case 'vietas':
				if (await GuildManager.updateRank(interaction.guild, interaction.options.getInteger('ranks', true), interaction.options.getRole('role'))) {
					await interaction.reply({
						ephemeral: true,
						content: `Atjaunots role ${interaction.options.getInteger('ranks')}. vietai!`,
					});
					return;
				}
				await interaction.reply({
					ephemeral: true,
					content: 'Kaut kas nestrādā!',
				});
				break;
			case 'calculate':
				await throwManager.calculate(interaction.guild);
				await interaction.reply({
					ephemeral: true,
					content: 'Done!',
				});
				return;
			default:
				logger.error(`Invalid subcommand "${interaction.options.getSubcommand()}" for ${this.name}`);
				await interaction.reply({
					ephemeral: true,
					content: 'Nav īstā komanda!',
				});
		}
	}
}

export default Config;