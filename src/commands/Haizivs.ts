import Base from './Base';
import { SlashCommandBuilder } from '@discordjs/builders';
import { CommandInteraction } from 'discord.js';

class Haizivs extends Base {
	name = 'haizivs';

	getCommand(): SlashCommandBuilder {
		const builder = new SlashCommandBuilder();
		builder.setName(this.name);
		builder.setDescription('Haizivs');
		return builder;
	}

	async execute(interaction: CommandInteraction) {
		await interaction.reply({
			content: 'https://i.imgur.com/Q66jDPi.png',
		});
	}
}

export default Haizivs;