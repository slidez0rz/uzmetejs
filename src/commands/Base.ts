import Command from '../interfaces/Command';
import Bot from '../Bot';
import { SlashCommandBuilder } from '@discordjs/builders';
import { CommandInteraction } from 'discord.js';

abstract class Base implements Command {
	public client!: Bot;
	abstract name: string;

	public constructor(client?: Bot) {
		if (client) {
			this.client = client;
		}
	}

	abstract getCommand(): SlashCommandBuilder;
	abstract execute(interaction: CommandInteraction): Promise<void>;
}

export default Base;