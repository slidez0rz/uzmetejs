import Base from './Base';
import { SlashCommandBuilder } from '@discordjs/builders';
import Severity from '../enums/Severity';
import { CommandInteraction, GuildMember, MessageEmbed, Permissions } from 'discord.js';
import { Throw, User } from '../models';
import ThrowAction from '../enums/ThrowAction';
import ThrowManager from '../managers/ThrowManager';

class AddThrow extends Base {
	name = 'uzmetiens';

	getCommand(): SlashCommandBuilder {
		const builder = new SlashCommandBuilder();
		builder.setName(this.name);
		builder.setDescription('Izveido jaunu uzmetienu izvēlētajam lietotājam');
		builder.addUserOption(option => option.setName('uzmetējs')
			.setDescription('Kurš uzmeta?')
			.setRequired(true));
		builder.addStringOption(option => option.setName('iemesls')
			.setDescription('Kāpēc uzmeta?')
			.setRequired(true));
		builder.addIntegerOption(option => option.setName('smagums')
			.setDescription('Cik smags uzmetiens?')
			.addChoice('Mazs', Severity.Low)
			.addChoice('Vidējs', Severity.Medium)
			.addChoice('Liels', Severity.High));
		return builder;
	}

	async execute(interaction: CommandInteraction) {
		if (!interaction.guildId || !interaction.guild) {
			await interaction.reply({
				ephemeral: true,
				content: 'WTF?',
			});
			return;
		}
		const member = interaction.options.getMember('uzmetējs', true) as GuildMember;
		if (member.user.bot) {
			await interaction.reply({
				ephemeral: true,
				content: 'Nevari uzmest botu!',
			});
			return;
		}
		await interaction.deferReply();
		const lastThrow = await Throw.findOne({
			order: [
				['id', 'DESC'],
			],
			limit: 1,
			include: [
				{
					association: 'author',
				},
			],
		});
		if (lastThrow && lastThrow.author?.guildMemberId === member.id && !interaction.memberPermissions?.has(Permissions.FLAGS.ADMINISTRATOR)) {
			await interaction.reply({
				content: 'Why you bully me!?',
			});
			return;
		}
		const reason = interaction.options.getString('iemesls', true);
		const severity = interaction.options.getInteger('smagums') || Severity.Low;
		const [userModel] = await User.findCreateFind({
			where: {
				guildMemberId: member.id,
			},
			defaults: {
				guildId: interaction.guildId,
				guildMemberId: member.id,
				guildMemberName: member.displayName,
			},
		});
		const authorMember = interaction.member as GuildMember;
		const [authorModel] = await User.findCreateFind({
			where: {
				guildMemberId: authorMember.id,
			},
			defaults: {
				guildId: interaction.guildId,
				guildMemberId: authorMember.id,
				guildMemberName: authorMember.displayName,
			},
		});
		const throwModel = await userModel.createThrow({
			guildId: interaction.guildId,
			userId: userModel.id,
			authorId: authorModel.id,
			severity: severity,
			reason: reason,
		});
		await throwModel.createLog({
			throwId: throwModel.id,
			guildId: interaction.guildId,
			authorId: authorModel.id,
			action: ThrowAction.Create,
		});
		await userModel.increment('points', {
			by: severity,
		});
		const severityTexts = [
			'',
			'nedaudz',
			'vidēji',
			'smagi',
		];
		await interaction.editReply({
			embeds: [
				new MessageEmbed()
					.setColor('RANDOM')
					.setTitle('Jauns uzmetiens!')
					.setDescription(`Izskatās, ka ${member} ir visus ${severityTexts[severity]} uzmetis!`)
					.addField('Iemesls', reason)
					.setTimestamp()
					.setFooter(`Uzmetiena ID #${throwModel.id}`),
			],
		});
		await ThrowManager.calculate(interaction.guild);
	}
}

export default AddThrow;