import Base from './Base';
import { SlashCommandBuilder } from '@discordjs/builders';
import { CommandInteraction, GuildMember, MessageEmbed } from 'discord.js';
import { Throw, User } from '../models';

class History extends Base {
	name = 'uzmetieni';

	getCommand(): SlashCommandBuilder {
		const builder = new SlashCommandBuilder();
		builder.setName(this.name);
		builder.setDescription('Parāda pēdējos uzmetienus');
		builder.addUserOption(option => option.setName('uzmetējs')
			.setDescription('Uzmetējs'));
		builder.addIntegerOption(option => option.setName('lapa')
			.setDescription('Lapa')
			.setMinValue(1));
		builder.addIntegerOption(option => option.setName('uzmetiens')
			.setDescription('Uzmetiena ID')
			.setMinValue(1));
		return builder;
	}

	async execute(interaction: CommandInteraction) {
		if (!interaction.guildId) {
			await interaction.reply({
				ephemeral: true,
				content: 'WTF?',
			});
			return;
		}
		const severityTexts = [
			'',
			'nedaudz',
			'vidēji',
			'smagi',
		];
		if (interaction.options.getInteger('uzmetiens')) {
			const throwModel = await Throw.findOne({
				where: {
					id: interaction.options.getInteger('uzmetiens'),
				},
				include: [
					{
						association: 'user',
					},
					{
						association: 'author',
					},
				],
			});
			if (!throwModel) {
				await interaction.reply({
					ephemeral: true,
					content: 'Uzmetiens nav atrasts',
				});
				return;
			}
			await interaction.reply({
				ephemeral: true,
				embeds: [
					new MessageEmbed()
						.setColor('RANDOM')
						.setTitle(`Uzmetiens #${throwModel.id}`)
						.setDescription(`Izskatās, ka ${throwModel.user?.guildMemberName} ir visus ${severityTexts[throwModel.severity]} uzmetis!`)
						.addField('Iemesls', throwModel.reason)
						.setTimestamp(throwModel.createdAt.getTime())
						.setFooter(`Uzmetienu reģistrēja ${throwModel.author?.guildMemberName}`),
				],
			});
			return;
		}
		const guildMember = interaction.options.getMember('uzmetējs');
		const page = interaction.options.getInteger('lapa') || 1;
		let where = {};
		if (guildMember && guildMember instanceof GuildMember) {
			const user = await User.findOne({
				where: {
					guildMemberId: guildMember.id,
				},
			});
			if (!user) {
				await interaction.reply({
					ephemeral: true,
					content: 'Uzmetējs nav atrasts',
				});
				return;
			}
			where = {
				userId: user.id,
			};
		}
		const throws = await Throw.findAndCountAll({
			where: where,
			order: [
				['created_at', 'DESC'],
			],
			limit: 5,
			offset: (page - 1) * 5,
			include: [
				{
					association: 'user',
				},
				{
					association: 'author',
				},
			],
		});
		if (!throws.rows.length || !throws.count) {
			await interaction.reply({
				ephemeral: true,
				content: 'Netika atrasts neviens uzmetiens',
			});
			return;
		}
		const embeds = [];
		throws.rows.forEach((throwModel: Throw) => {
			const embed = new MessageEmbed()
				.setColor('RANDOM')
				.setTitle(`Uzmetiens #${throwModel.id}`)
				.setDescription(`Izskatās, ka ${throwModel.user?.guildMemberName} ir visus ${severityTexts[throwModel.severity]} uzmetis!`)
				.addField('Iemesls', throwModel.reason);
			if (throwModel.isDeleted) {
				embed.addField('Uzmetiena statuss', 'Dzēsts');
			}
			embeds.push(embed);
		});

		embeds.push(new MessageEmbed()
			.setColor('RANDOM')
			.setDescription(`${page}. lapa no ${Math.ceil(throws.count / 5)}`));

		await interaction.reply({
			ephemeral: true,
			embeds: embeds,
		});
	}
}

export default History;