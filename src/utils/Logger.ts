import pino from 'pino';

const transport = pino.transport({
	targets: [
		{
			target: 'pino/file',
			options: {
				destination: 'logs/error.log',
			},
			level: 'warn',
		},
		{
			target: 'pino-pretty',
			options: {
				destination: 1,
				translateTime: true,
				ignore: 'pid,hostname',
			},
			level: 'trace',
		},
	],
});
const logger = pino(transport);

export default logger;