import { Client, Collection } from 'discord.js';
import fs from 'fs';
import CommandBase from '../commands/Base';

export const loadCommands = async (client?: Client) => {
	const commands = new Collection<string, CommandBase>();
	const files = fs.readdirSync(`${__dirname}/../commands`)
		.filter(file => !file.startsWith('Base'));
	for (const file of files) {
		const { default: command } = await import(`${__dirname}/../commands/${file}`);
		const instance = new command(client);
		commands.set(instance.name, instance);
	}
	return commands;
};

export const registerEvents = async (client: Client) => {
	const files = fs.readdirSync(`${__dirname}/../events`)
		.filter(file => !file.startsWith('Base'));
	for (const file of files) {
		const { default: event } = await import(`${__dirname}/../events/${file}`);
		const instance = new event(client);
		client.on(instance.event, instance.execute.bind(instance));
	}
};