import { Client, Collection, Guild, Intents, Interaction } from 'discord.js';
import logger from './utils/Logger';
import { loadCommands, registerEvents } from './utils/Helpers';
import Base from './commands/Base';
import GuildManager from './managers/GuildManager';
import ThrowManager from './managers/ThrowManager';

export default class Bot extends Client {
	private loginTries = 0;
	private commands = new Collection<string, Base>();

	public constructor() {
		super({
			intents: [
				Intents.FLAGS.GUILDS,
			],
		});
	}

	public async init() {
		GuildManager.setClient(this);
		ThrowManager.setClient(this);
		this.commands = await loadCommands(this);
		await registerEvents(this);

		this.once('ready', this.onReady);

		this.on('interactionCreate', async (interaction: Interaction) => {
			if (!interaction.isCommand()) {
				return;
			}

			const command = this.commands.get(interaction.commandName);

			if (!command) {
				logger.warn(`Invalid command "${interaction.commandName}" specified`);
				return;
			}

			try {
				await command.execute(interaction);
			} catch (error) {
				logger.error(error);
				await interaction.reply({
					content: 'There was an error while executing this command!',
					ephemeral: true,
				});
			}
		});

		this.tryLogin();
	}

	private tryLogin() {
		this.loginTries++;
		super.login(process.env.BOT_TOKEN).catch(e => {
			if (this.loginTries < 3) {
				this.tryLogin();
			} else {
				logger.error(e.message);
				logger.info('Failed to authorize bot. Retrying in 60 seconds');
				this.loginTries = 0;
				setTimeout(this.tryLogin, 60000);
			}
		});
	}

	private async onReady() {
		this.guilds.cache.each((guild: Guild) => {
			GuildManager.init(guild);
		});
		logger.info('Ready!');
	}
}