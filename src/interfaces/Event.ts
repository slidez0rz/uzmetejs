import { Base } from 'discord.js';

interface Event {
	event: string;
	execute(...args: Base[]): Promise<void>;
}

export default Event;