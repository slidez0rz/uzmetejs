import { Sequelize } from 'sequelize';
import logger from '../utils/Logger';

const sequelize = new Sequelize({
	dialect: 'sqlite',
	storage: './database.sqlite',
	logging: sql => logger.info(sql),
});

export default sequelize;