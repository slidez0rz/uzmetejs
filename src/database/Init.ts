import sequelize from './Connection';
import Configuration from '../models/Configuration';
import { User, Throw, ThrowLog } from '../models';
import logger from '../utils/Logger';

const force = process.argv.includes('--force') || process.argv.includes('-f');

Promise.all([
	Configuration.sync({ force }),
	User.sync({ force }),
	Throw.sync({ force }),
	ThrowLog.sync({ force }),
]).then(async () => {
	logger.info('Database synced');
	await sequelize.close();
}).catch(e => logger.error(e.message));