import { DataTypes, Model, Optional } from 'sequelize';
import sequelize from '../database/Connection';

interface RankRole {
	rank: number;
	roleId: string;
}

interface ConfigurationAttributes {
	id: number;
	guildId: string;
	guildName: string;
	isEnabled: boolean;
	rankRoles: Array<RankRole>;
}

type ConfigurationCreationAttributes = Optional<ConfigurationAttributes, 'id' | 'guildName' | 'isEnabled' | 'rankRoles'>

class Configuration extends Model<ConfigurationAttributes, ConfigurationCreationAttributes> {
	public id!: number;
	public guildId!: string;
	public guildName!: string;
	public isEnabled!: boolean;
	public rankRoles!: Array<RankRole>;

	public readonly createdAt!: Date;
	public readonly updatedAt!: Date;
}

Configuration.init({
	id: {
		type: DataTypes.BIGINT.UNSIGNED,
		primaryKey: true,
		autoIncrement: true,
	},
	guildId: {
		field: 'guild_id',
		type: DataTypes.STRING,
		allowNull: false,
	},
	guildName: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	isEnabled: {
		field: 'is_enabled',
		type: DataTypes.BOOLEAN,
		defaultValue: false,
	},
	rankRoles: {
		field: 'rank_roles',
		type: DataTypes.JSON,
		defaultValue: '[]',
		get: function() {
			// @ts-ignore
			return JSON.parse(this.getDataValue('rankRoles'));
		},
		set: function(value: Array<RankRole>) {
			// @ts-ignore
			return this.setDataValue('rankRoles', JSON.stringify(value));
		},
	},
}, {
	sequelize: sequelize,
	tableName: 'configurations',
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	indexes: [
		{
			fields: [
				'guild_id',
			],
			unique: true,
		},
	],
});

export default Configuration;