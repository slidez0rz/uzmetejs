import { DataTypes, Optional, Model, Association } from 'sequelize';
import sequelize from '../database/Connection';
import ThrowAction from '../enums/ThrowAction';
import User from './User';
import Throw from './Throw';

interface ThrowLogAttributes {
	id: number;
	guildId: string;
	throwId: number;
	authorId: number;
	action: ThrowAction;
}

type ThrowLogCreationAttributes = Optional<ThrowLogAttributes, 'id'>

class ThrowLog extends Model<ThrowLogAttributes, ThrowLogCreationAttributes> {
	public id!: number;
	public guildId!: string;
	public throwId!: number;
	public authorId!: number;
	public action!: ThrowAction;

	public readonly createdAt!: Date;
	public readonly updatedAt!: Date;

	public readonly author?: User;
	public readonly throw?: Throw;

	public static associations: {
		author: Association<ThrowLog, User>;
		throw: Association<ThrowLog, Throw>;
	};
}

ThrowLog.init({
	id: {
		type: DataTypes.BIGINT.UNSIGNED,
		primaryKey: true,
		autoIncrement: true,
	},
	guildId: {
		field: 'guild_id',
		type: DataTypes.STRING,
		allowNull: false,
	},
	throwId: {
		field: 'throw_id',
		type: DataTypes.BIGINT.UNSIGNED,
		allowNull: false,
	},
	authorId: {
		field: 'author_id',
		type: DataTypes.BIGINT.UNSIGNED,
		allowNull: false,
	},
	action: {
		type: DataTypes.ENUM,
		values: Object.values(ThrowAction).map(String).slice(Object.values(ThrowAction).length / 2),
		allowNull: false,
	},
}, {
	sequelize: sequelize,
	tableName: 'throw_logs',
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	indexes: [
		{
			fields: [
				'guild_id',
				'created_at',
			],
		},
		{
			fields: [
				'author_id',
				'created_at',
			],
		},
	],
});

export default ThrowLog;