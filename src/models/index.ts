import Configuration from './Configuration';
import Throw from './Throw';
import ThrowLog from './ThrowLog';
import User from './User';

User.hasMany(Throw, {
	sourceKey: 'id',
	foreignKey: 'user_id',
	as: 'throws',
});

User.hasMany(ThrowLog, {
	sourceKey: 'id',
	foreignKey: 'user_id',
	as: 'actions',
});

Throw.belongsTo(User, {
	foreignKey: 'user_id',
	as: 'user',
});

Throw.belongsTo(User, {
	foreignKey: 'author_id',
	as: 'author',
});

Throw.hasMany(ThrowLog, {
	sourceKey: 'id',
	foreignKey: 'throw_id',
	as: 'logs',
});

ThrowLog.belongsTo(Throw, {
	foreignKey: 'throw_id',
	as: 'throw',
});

ThrowLog.belongsTo(User, {
	foreignKey: 'author_id',
	as: 'author',
});

export { Configuration, User, Throw, ThrowLog };