import { DataTypes, Optional, Model, Association, HasManyCreateAssociationMixin } from 'sequelize';
import sequelize from '../database/Connection';
import Throw from './Throw';
import ThrowLog from './ThrowLog';

interface UserAttributes {
	id: number;
	guildId: string;
	guildMemberId: string;
	guildMemberName: string;
	points: number;
}

type UserCreationAttributes = Optional<UserAttributes, 'id' | 'points'>

class User extends Model<UserAttributes, UserCreationAttributes> {
	public id!: number;
	public guildId!: string;
	public guildMemberId!: string;
	public guildMemberName!: string;
	public points!: number;

	public readonly createdAt!: Date;
	public readonly updatedAt!: Date;

	public createThrow!: HasManyCreateAssociationMixin<Throw>;

	public readonly throws?: Throw[];

	public static associations: {
		throws: Association<User, Throw>;
		actions: Association<User, ThrowLog>;
	};
}

User.init({
	id: {
		type: DataTypes.BIGINT.UNSIGNED,
		primaryKey: true,
		autoIncrement: true,
	},
	guildId: {
		field: 'guild_id',
		type: DataTypes.STRING,
		allowNull: false,
	},
	guildMemberId: {
		field: 'guild_member_id',
		type: DataTypes.STRING,
		allowNull: false,
	},
	guildMemberName: {
		field: 'guild_member_name',
		type: DataTypes.STRING,
		allowNull: false,
	},
	points: {
		type: DataTypes.INTEGER.UNSIGNED,
		defaultValue: 0,
	},
}, {
	sequelize: sequelize,
	tableName: 'users',
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	indexes: [
		{
			fields: [
				'guild_id',
				'points',
			],
		},
		{
			fields: [
				'guild_member_id',
			],
			unique: true,
		},
	],
});

export default User;