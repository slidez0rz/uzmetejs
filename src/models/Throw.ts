import { DataTypes, Optional, Model, Association, HasManyCreateAssociationMixin } from 'sequelize';
import sequelize from '../database/Connection';
import Severity from '../enums/Severity';
import User from './User';
import ThrowLog from './ThrowLog';

interface ThrowAttributes {
	id: number;
	guildId: string;
	userId: number;
	authorId: number;
	reason: string;
	severity: Severity;
	isDeleted: boolean;
	createdAt: Date;
}

type ThrowCreationAttributes = Optional<ThrowAttributes, 'id' | 'isDeleted' | 'createdAt'>

class Throw extends Model<ThrowAttributes, ThrowCreationAttributes> {
	public id!: number;
	public guildId!: string;
	public userId!: number;
	public authorId!: number;
	public reason!: string;
	public severity!: Severity;
	public isDeleted!: boolean;

	public readonly createdAt!: Date;
	public readonly updatedAt!: Date;

	public createLog!: HasManyCreateAssociationMixin<ThrowLog>;

	public readonly user?: User;
	public readonly author?: User;
	public readonly logs?: ThrowLog[];

	public static associations: {
		user: Association<Throw, User>;
		author: Association<Throw, User>;
		logs: Association<Throw, ThrowLog>;
	};
}

Throw.init({
	id: {
		type: DataTypes.BIGINT.UNSIGNED,
		primaryKey: true,
		autoIncrement: true,
	},
	guildId: {
		field: 'guild_id',
		type: DataTypes.STRING,
		allowNull: false,
	},
	userId: {
		field: 'user_id',
		type: DataTypes.BIGINT.UNSIGNED,
		allowNull: false,
	},
	authorId: {
		field: 'author_id',
		type: DataTypes.BIGINT.UNSIGNED,
		allowNull: false,
	},
	reason: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	severity: {
		type: DataTypes.ENUM,
		values: Object.values(Severity).map(String).slice(Object.values(Severity).length / 2),
		allowNull: false,
	},
	isDeleted: {
		field: 'is_deleted',
		type: DataTypes.BOOLEAN,
		defaultValue: false,
	},
	createdAt: {
		field: 'created_at',
		type: DataTypes.DATE,
	},
}, {
	sequelize: sequelize,
	tableName: 'throws',
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	indexes: [
		{
			fields: [
				'guild_id',
				'created_at',
			],
		},
		{
			fields: [
				'user_id',
				'created_at',
			],
		},
		{
			fields: [
				'author_id',
				'created_at',
			],
		},
	],
});

export default Throw;