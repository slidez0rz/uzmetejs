import Bot from '../Bot';
import { Guild, Role } from 'discord.js';
import { Configuration } from '../models';
import { APIRole, Routes } from 'discord-api-types/v9';
import { loadCommands } from '../utils/Helpers';
import Base from '../commands/Base';
import { globalCommands } from '../utils/Values';
import { REST } from '@discordjs/rest';
import logger from '../utils/Logger';

const token = String(process.env.BOT_TOKEN);
const clientId = String(process.env.CLIENT_ID);

class GuildManager {
	public client!: Bot;

	public setClient(client: Bot) {
		this.client = client;
	}

	public async init(guild: Guild) {
		let config = await Configuration.findOne({
			where: {
				guildId: guild.id,
			},
		});
		if (!config) {
			config = await Configuration.create({
				guildId: guild.id,
				guildName: guild.name,
			});
		}
		if (!config.isEnabled) {
			await this.unregisterGuildCommands(guild);
			return;
		}
		await this.registerGuildCommands(guild);
	}

	public async enable(guild: Guild): Promise<boolean> {
		const config = await Configuration.findOne({
			where: {
				guildId: guild.id,
			},
		});
		if (!config) {
			return false;
		}
		await config.update({
			isEnabled: true,
		});
		await this.registerGuildCommands(guild);
		return true;
	}

	public async disable(guild: Guild): Promise<boolean> {
		const config = await Configuration.findOne({
			where: {
				guildId: guild.id,
			},
		});
		if (!config) {
			return false;
		}
		await config.update({
			isEnabled: false,
		});
		await this.unregisterGuildCommands(guild);
		return true;
	}

	public async updateRank(guild: Guild, rank: number, role: Role | APIRole | null) {
		const config = await Configuration.findOne({
			where: {
				guildId: guild.id,
			},
		});
		if (!config) {
			return false;
		}
		const rankRoles = config.rankRoles;
		const currentIndex = config.rankRoles.findIndex(rankRole => rankRole.rank === rank);
		if (role === null) {
			if (currentIndex) {
				return true;
			}
			rankRoles.splice(currentIndex, 1);
			await config.update({
				rankRoles: rankRoles,
			});
			return true;
		}
		const rankRole = {
			rank: rank,
			roleId: role.id,
		};
		if (currentIndex === -1) {
			rankRoles.push(rankRole);
		} else {
			rankRoles[currentIndex] = rankRole;
		}
		rankRoles.sort((a, b) => {
			return a.rank - b.rank;
		});
		await config.update({
			rankRoles: rankRoles,
		});
		return true;
	}

	private async registerGuildCommands(guild: Guild) {
		const rest = new REST({
			version: '9',
		}).setToken(token);

		const commands = (await loadCommands())
			.filter((command: Base) => !globalCommands.includes(command.name))
			.map((command: Base) => command.getCommand().toJSON());

		rest.put(Routes.applicationGuildCommands(clientId, guild.id), {
			body: commands,
		}).then(() => {
			logger.info(`Guild commands registered for guild ${guild.name}`);
		}).catch(e => {
			logger.info(e.message);
		});
	}

	private unregisterGuildCommands(guild: Guild) {
		const rest = new REST({
			version: '9',
		}).setToken(token);

		rest.put(Routes.applicationGuildCommands(clientId, guild.id), {
			body: [],
		}).then(() => {
			logger.info(`Guild commands unregistered for guild ${guild.name}`);
		}).catch(e => {
			logger.info(e.message);
		});
	}
}

const guildManager = new GuildManager();

export default guildManager;