import Bot from '../Bot';
import { Guild, GuildMember } from 'discord.js';
import { Configuration, Throw, User } from '../models';
import logger from '../utils/Logger';
import { Op } from 'sequelize';

class ThrowManager {
	public client!: Bot;

	public setClient(client: Bot) {
		this.client = client;
	}

	public async calculate(guild: Guild) {
		const config = await Configuration.findOne({
			where: {
				guildId: guild.id,
			},
		});
		if (!config) {
			return;
		}
		const activeUsers = await User.findAll({
			where: {
				guildId: guild.id,
			},
			order: [
				['points', 'DESC'],
				['updated_at', 'DESC'],
			],
		});
		for (const user of activeUsers) {
			let member;
			try {
				member = await guild.members.fetch(user.guildMemberId);
				if (!member) {
					continue;
				}
			} catch (e) {
				logger.error(e);
				continue;
			}
			const rank = activeUsers.findIndex(activeUser => activeUser.id === user.id) + 1;
			await this.setMemberRankRoles(config, member, user, rank);
		}
	}

	public async setRole(member: GuildMember) {
		const config = await Configuration.findOne({
			where: {
				guildId: member.guild.id,
			},
		});
		if (!config) {
			return;
		}
		const user = await User.findOne({
			where: {
				guildMemberId: member.id,
			},
		});
		if (!user) {
			return;
		}
		if (!user.points) {
			return;
		}
		const rank = (await User.count({
			where: {
				guildId: member.guild.id,
				[Op.or]: [
					{
						points: {
							[Op.gt]: user.points,
						},
					},
					{
						points: user.points,
						updated_at: {
							[Op.gt]: user.updatedAt,
						},
					},
				],
			},
		})) + 1;
		await this.setMemberRankRoles(config, member, user, rank);
	}

	private async setMemberRankRoles(config: Configuration, member: GuildMember, user: User, rank: number) {
		const roleIds = config.rankRoles.map(rankRole => rankRole.roleId);
		const activeRankRole = config.rankRoles.find(rankRole => rankRole.rank === rank);
		let rolesToRemove = roleIds;
		if (activeRankRole && user.points) {
			rolesToRemove = rolesToRemove.filter(roleId => roleId !== activeRankRole.roleId);
		}
		try {
			await member.roles.remove(rolesToRemove);
			if (activeRankRole && user.points) {
				await member.roles.add(activeRankRole.roleId);
			}
		} catch (e) {
			logger.error(e);
		}
	}

	public async removeThrow(throwModel: Throw) {
		const points = throwModel.severity;
		await throwModel.update({
			isDeleted: true,
		});
		await throwModel.user?.decrement('points', {
			by: points,
		});
		const guild = await this.client.guilds.fetch(throwModel.guildId);
		if (!guild) {
			return;
		}
		await this.calculate(guild);
	}
}

const throwManager = new ThrowManager();

export default throwManager;