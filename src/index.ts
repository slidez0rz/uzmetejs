import dotenv from 'dotenv';
dotenv.config();
import Bot from './Bot';
import logger from './utils/Logger';

new Bot().init().catch(e => logger.error(e.message));