import Base from './Base';
import { Guild as DiscordGuild } from 'discord.js';
import GuildManager from '../managers/GuildManager';
import logger from '../utils/Logger';

class GuildCreate extends Base {
	public event = 'guildCreate';

	public async execute(guild: DiscordGuild) {
		logger.info(`Bot joined guild ${guild.name}`);
		await GuildManager.init(guild);
	}
}

export default GuildCreate;