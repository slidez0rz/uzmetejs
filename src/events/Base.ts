import Event from '../interfaces/Event';
import Bot from '../Bot';
import { Base as SnowflakeBase } from 'discord.js';

abstract class Base implements Event {
	public client!: Bot;
	abstract event: string;

	public constructor(client?: Bot) {
		if (client) {
			this.client = client;
		}
	}
	abstract execute(...args: SnowflakeBase[]): Promise<void>;
}

export default Base;