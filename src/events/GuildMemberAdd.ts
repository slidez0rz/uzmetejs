import Base from './Base';
import { GuildMember } from 'discord.js';
import logger from '../utils/Logger';
import ThrowManager from '../managers/ThrowManager';

class GuildMemberAdd extends Base {
	public event = 'guildCreate';

	public async execute(guildMember: GuildMember) {
		if (guildMember.user.bot) {
			return;
		}
		logger.info(`${guildMember.nickname || guildMember.displayName} joined guild ${guildMember.guild.name}`);
		await ThrowManager.setRole(guildMember);
	}
}

export default GuildMemberAdd;