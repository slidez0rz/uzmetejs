enum ThrowAction {
	Create = 'create',
	Delete = 'delete',
}

export default ThrowAction;