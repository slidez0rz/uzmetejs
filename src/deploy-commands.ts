import dotenv from 'dotenv';
dotenv.config();
import { REST } from '@discordjs/rest';
import logger from './utils/Logger';
import { loadCommands } from './utils/Helpers';
import Base from './commands/Base';
import { Routes } from 'discord-api-types/v9';
import { globalCommands } from './utils/Values';

if (!process.env.BOT_TOKEN) {
	logger.debug('No BOT_TOKEN set!');
	process.exit(1);
}

if (!process.env.CLIENT_ID) {
	logger.debug('No BOT_TOKEN set!');
	process.exit(1);
}

const token = String(process.env.BOT_TOKEN);
const clientId = String(process.env.CLIENT_ID);

const rest = new REST({
	version: '9',
}).setToken(token);

(async function() {
	const commands = await loadCommands();

	rest.put(Routes.applicationCommands(clientId), {
		body: commands
			.filter((command: Base) => globalCommands.includes(command.name))
			.map((command: Base) => command.getCommand().toJSON()),
	}).then(() => {
		logger.info('Global commands registered');
	}).catch(e => {
		logger.info(e.message);
	});
})();